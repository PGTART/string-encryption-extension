﻿
// this code extends the default string type with an option to Encrypt or Decrypt
// with Rijndael encryption, based upon http://tekeye.biz/2015/encrypt-decrypt-c-sharp-string
// Fixed By Peter Boos against padding errors, and combined into a String extension method
//
// As a string extension Method, this becomes very easy to use in your code..
//
//  String S="Peter Boos";
//  S.Encode("phasephrase"); // phasephare can be anything
//  S.Decode("phasephrase"); 
//

// Extension methods are public Static classes 
// The can be in their own class lib but then make sure your type
// Using StringEncrypt in the parts of your program where you want to use it.
//
//
//  On a per project base i recomend to alter the initvector (but dont loose them!!).
//
//  Note that reverse enginering C# is always possible, though with this
//  Its more complex to see whats inside settings.
//  Note in the end the resulting coded string is always in HEX; 
//  they are long but can be used everywhere.


using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Linq;

public static class StringEncrypt
{

    private static byte[] salt = Encoding.ASCII.GetBytes("somerandomstuff");


  

    public static string EncryptString(this string plainText, string keyString)
    {
        Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(keyString, salt);

        MemoryStream ms = new MemoryStream();
        StreamWriter sw = new StreamWriter(new CryptoStream(ms, new RijndaelManaged().CreateEncryptor(key.GetBytes(32), key.GetBytes(16)), CryptoStreamMode.Write));
        sw.Write(plainText);
        sw.Close();
        ms.Close();
        return Convert.ToBase64String(ms.ToArray());


    }


    public static string DecryptString(this string cipherText, string keyString)
    {

        try
        {
            Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(keyString, salt);

            ICryptoTransform d = new RijndaelManaged().CreateDecryptor(key.GetBytes(32), key.GetBytes(16));
            byte[] bytes = Convert.FromBase64String(cipherText);

            cipherText = new StreamReader(new CryptoStream(new MemoryStream(bytes), d, CryptoStreamMode.Read)).ReadToEnd();

            return cipherText;
          //  return new StreamReader(new CryptoStream(new MemoryStream(bytes), d, CryptoStreamMode.Read)).ReadToEnd();
        }
        catch
        {
            return "invalid chipper text or key string";
        }


    }

    private static string ToHexString(string str)
    {
        var sb = new StringBuilder();
        byte[] bytes = Encoding.Unicode.GetBytes(str);
        foreach (var t in bytes)
        {
            sb.Append(t.ToString("X2"));
        }
        
        return sb.ToString();
    }


    private static string FromHexString(string hexString) // to read
    {
        var bytes = new byte[hexString.Length / 2];
        for (var i = 0; i < bytes.Length; i++)
        {
            bytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
        }
        return Encoding.Unicode.GetString(bytes);
    }
}


class demoProgam
{
    static void Main()
    {
        string key = "Use" + "some" +"math" +"Where" +"Keys are used"+"for a key";
        // finding plain strings in source code is too easy..

        string s = "Peter .09==Boost <>?";
        Console.WriteLine(s);
        s = s.EncryptString(key);
        Console.WriteLine("Encrypted : " +s );
        
        Console.WriteLine("Decrypted : " + s.DecryptString(key));
        String q = "FocjbxjRx4fnmYNaden9+tbcvqKXIjgjqumLIEXNyEw=";
        Console.WriteLine("Decrypted : " +q+ "\n" + q.DecryptString(key));


        Console.WriteLine("Enter some text to encrypt");
        s=Console.ReadLine();
        Console.WriteLine("Enter a phrase key of normal characters");
        key = Console.ReadLine();
        Console.WriteLine(s.EncryptString(key));
        Console.WriteLine(s.DecryptString(key));
        Console.WriteLine("Ready");
        Console.ReadLine();
    }
}